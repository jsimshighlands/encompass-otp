﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.Diagnostics;
using Newtonsoft.Json;
using EllieMae.Encompass.ComponentModel;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Users;
using QRCoder;
using OtpNet;

namespace encompass_login_plugin
{
    public partial class OTPForm : Form
    {
        readonly QRCodeGenerator qrGenerator = new QRCodeGenerator();
        private int tryCounter;

        public OTPForm()
        {
            InitializeComponent();
        }

        private void OTPForm_Load(object sender, EventArgs e)
        {
            /*
             * Form load event that will setup the code and other information
             * needed for the authenticator. 
             */

            //variables to be used for QR code and form
            var otpSecret = Base32Encoding.ToString(KeyGeneration.GenerateRandomKey(20));
            var issuer = String.Concat("Encompass: ", GetClientID());
            var label = Global.user.ID.ToLower();

            //QRCode URI to be sued in QRCode
            var qrCodeUri = $"otpauth://totp/{Uri.EscapeDataString(label)}?secret={otpSecret}&issuer={Uri.EscapeDataString(issuer)}";

            //generate QR Code
            using (var qrCodeData = qrGenerator.CreateQrCode(qrCodeUri, QRCodeGenerator.ECCLevel.Q))
            {
                using (var qrCode = new QRCode(qrCodeData))
                {
                    var qrCodeImage = qrCode.GetGraphic(20);

                    using (var memory = new MemoryStream())
                    {
                        qrCodeImage.Save(memory, ImageFormat.Bmp);
                        memory.Position = 0;
                        var bitmapImage = new BitmapImage();
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource = memory;
                        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                        bitmapImage.EndInit();
                        pbxQRCode.Image = BitmapImage2Bitmap(bitmapImage);
                    }
                }
            }

            txtIssuer.Text = issuer;
            txtLabel.Text = label;
            txtSecret.Text = otpSecret;
        }

        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            /*
             * Helper method to change a BitmapImage to a Bitmap
             * this is needed so that it can be used with the
             * forms picture box.
             */

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                Bitmap bitmap = new Bitmap(outStream);

                return new Bitmap(bitmap);
            }
        }

        private void btnSetupVerify_Click(object sender, EventArgs e)
        {
            // Generate new code from secret code in the Secrets Textbox
            var totp = new Totp(Base32Encoding.ToBytes(txtSecret.Text));

            // Check to verify that the code provided by the user is valid
            var isValidCode = totp.VerifyTotp(txtVerifyCode.Text, out long timeSetupMatched, VerificationWindow.RfcSpecifiedNetworkDelay);

            if (isValidCode)
            {
                /* If the code provided by the user is valid create a new Data Object
                 * saving the Security Key from the Secret textbox, the security code
                 * used to login.
                */
                try
                {
                    UserDataObject userDataObject = new UserDataObject();
                    userDataObject.SecurityKey = txtSecret.Text;
                    List<long> timeMaches = new List<long> { timeSetupMatched };
                    userDataObject.TimeStepMatch = timeMaches;

                    string json = JsonConvert.SerializeObject(userDataObject);
                    byte[] bytes = Encoding.UTF8.GetBytes(json);

                    var encDataObject = new EllieMae.Encompass.BusinessObjects.DataObject();
                    encDataObject.Load(bytes);

                    Global.user.SaveCustomDataObject(Global.userOTPDataObjectName, encDataObject);

                    DialogResult result = MessageBox.Show("Code is valid. Clicking OK will close out this prompt.", "Valid Code", MessageBoxButtons.OK);

                    if (result == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    throw;
                }
            }
            else
            {
                /* If the code is not valid then only allow 3 times to
                 * provide correct code. If the correct code is not provided
                 * after the 3rd incorrect code Encompass closes.
                */
                if (tryCounter <= 2)
                {
                    MessageBox.Show("Invalid Code");
                    tryCounter += 1;
                }
                else
                {
                    MessageBox.Show("This code is not valid, please try again", "Invalid Code", MessageBoxButtons.OK);

                    //Gets the Encompass process and then kills that process.
                    Process p = Process.GetCurrentProcess();
                    p.Kill();
                }
            }
        }

        private string GetClientID()
        {
            /* Returns only the Client ID of the system, did this for
             * this for systems where Test and Prod share a client ID
             * example: Prod - BE123456 Test - TEBE123456a
             */
            try
            {
                var serverURI = EncompassApplication.Session.ServerURI;
                return serverURI.Substring(serverURI.IndexOf("$") + 1).ToLower();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
            
        }

        private void OTPForm_KeyDown(object sender, KeyEventArgs e)
        {
            /* Allows for users to use the enter key to trigger the
             * Setup Verify button
            */
            if (e.KeyCode == Keys.Enter)
            {
                btnSetupVerify_Click(this, new EventArgs());
            }
            else if (e.Alt && e.KeyCode == Keys.F4)
            {
                e.Handled = true;
            }
        }
    }
}
