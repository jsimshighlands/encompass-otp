﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace encompass_login_plugin
{
    public class UserDataObject
    {
        /*
         * Class to use for a user customd data object
         * json serilization and deserilization
         */
        public string SecurityKey { get; set; }
        public List<long> TimeStepMatch { get; set; }
    }
}
