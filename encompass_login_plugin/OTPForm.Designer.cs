﻿
namespace encompass_login_plugin
{
    partial class OTPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbxQRCode = new System.Windows.Forms.PictureBox();
            this.lblIssuer = new System.Windows.Forms.Label();
            this.lblLabel = new System.Windows.Forms.Label();
            this.lblSecret = new System.Windows.Forms.Label();
            this.txtIssuer = new System.Windows.Forms.TextBox();
            this.txtLabel = new System.Windows.Forms.TextBox();
            this.txtSecret = new System.Windows.Forms.TextBox();
            this.txtVerifyCode = new System.Windows.Forms.TextBox();
            this.lblVerifyCode = new System.Windows.Forms.Label();
            this.btnSetupVerify = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbxQRCode)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxQRCode
            // 
            this.pbxQRCode.Location = new System.Drawing.Point(150, 10);
            this.pbxQRCode.Name = "pbxQRCode";
            this.pbxQRCode.Size = new System.Drawing.Size(200, 200);
            this.pbxQRCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxQRCode.TabIndex = 0;
            this.pbxQRCode.TabStop = false;
            // 
            // lblIssuer
            // 
            this.lblIssuer.AutoSize = true;
            this.lblIssuer.Location = new System.Drawing.Point(75, 230);
            this.lblIssuer.Name = "lblIssuer";
            this.lblIssuer.Size = new System.Drawing.Size(35, 13);
            this.lblIssuer.TabIndex = 1;
            this.lblIssuer.Text = "Issuer";
            // 
            // lblLabel
            // 
            this.lblLabel.AutoSize = true;
            this.lblLabel.Location = new System.Drawing.Point(77, 256);
            this.lblLabel.Name = "lblLabel";
            this.lblLabel.Size = new System.Drawing.Size(33, 13);
            this.lblLabel.TabIndex = 2;
            this.lblLabel.Text = "Label";
            // 
            // lblSecret
            // 
            this.lblSecret.AutoSize = true;
            this.lblSecret.Location = new System.Drawing.Point(72, 282);
            this.lblSecret.Name = "lblSecret";
            this.lblSecret.Size = new System.Drawing.Size(38, 13);
            this.lblSecret.TabIndex = 3;
            this.lblSecret.Text = "Secret";
            // 
            // txtIssuer
            // 
            this.txtIssuer.Enabled = false;
            this.txtIssuer.Location = new System.Drawing.Point(116, 227);
            this.txtIssuer.Name = "txtIssuer";
            this.txtIssuer.Size = new System.Drawing.Size(275, 20);
            this.txtIssuer.TabIndex = 4;
            // 
            // txtLabel
            // 
            this.txtLabel.Enabled = false;
            this.txtLabel.Location = new System.Drawing.Point(116, 253);
            this.txtLabel.Name = "txtLabel";
            this.txtLabel.Size = new System.Drawing.Size(275, 20);
            this.txtLabel.TabIndex = 5;
            // 
            // txtSecret
            // 
            this.txtSecret.Enabled = false;
            this.txtSecret.Location = new System.Drawing.Point(116, 279);
            this.txtSecret.Name = "txtSecret";
            this.txtSecret.Size = new System.Drawing.Size(275, 20);
            this.txtSecret.TabIndex = 6;
            // 
            // txtVerifyCode
            // 
            this.txtVerifyCode.Location = new System.Drawing.Point(116, 337);
            this.txtVerifyCode.Name = "txtVerifyCode";
            this.txtVerifyCode.Size = new System.Drawing.Size(275, 20);
            this.txtVerifyCode.TabIndex = 7;
            this.txtVerifyCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OTPForm_KeyDown);
            // 
            // lblVerifyCode
            // 
            this.lblVerifyCode.AutoSize = true;
            this.lblVerifyCode.Location = new System.Drawing.Point(220, 317);
            this.lblVerifyCode.Name = "lblVerifyCode";
            this.lblVerifyCode.Size = new System.Drawing.Size(61, 13);
            this.lblVerifyCode.TabIndex = 8;
            this.lblVerifyCode.Text = "Verify Code";
            // 
            // btnSetupVerify
            // 
            this.btnSetupVerify.Location = new System.Drawing.Point(210, 363);
            this.btnSetupVerify.Name = "btnSetupVerify";
            this.btnSetupVerify.Size = new System.Drawing.Size(75, 23);
            this.btnSetupVerify.TabIndex = 9;
            this.btnSetupVerify.Text = "Verify";
            this.btnSetupVerify.UseVisualStyleBackColor = true;
            this.btnSetupVerify.Click += new System.EventHandler(this.btnSetupVerify_Click);
            // 
            // OTPForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 411);
            this.ControlBox = false;
            this.Controls.Add(this.btnSetupVerify);
            this.Controls.Add(this.lblVerifyCode);
            this.Controls.Add(this.txtVerifyCode);
            this.Controls.Add(this.txtSecret);
            this.Controls.Add(this.txtLabel);
            this.Controls.Add(this.txtIssuer);
            this.Controls.Add(this.lblSecret);
            this.Controls.Add(this.lblLabel);
            this.Controls.Add(this.lblIssuer);
            this.Controls.Add(this.pbxQRCode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Name = "OTPForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Authenticator Setup";
            this.Load += new System.EventHandler(this.OTPForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OTPForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pbxQRCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxQRCode;
        private System.Windows.Forms.Label lblIssuer;
        private System.Windows.Forms.Label lblLabel;
        private System.Windows.Forms.Label lblSecret;
        private System.Windows.Forms.TextBox txtIssuer;
        private System.Windows.Forms.TextBox txtLabel;
        private System.Windows.Forms.TextBox txtSecret;
        private System.Windows.Forms.TextBox txtVerifyCode;
        private System.Windows.Forms.Label lblVerifyCode;
        private System.Windows.Forms.Button btnSetupVerify;
    }
}