﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using OtpNet;

namespace encompass_login_plugin
{
    public partial class OTPVerify : Form
    {
        private int tryCounter = 0;

        public OTPVerify()
        {
            InitializeComponent();
        }

        private void btnVerifyCode_Click(object sender, EventArgs e)
        {
            /*
             * Retrieves the users custom data object and use the 
             * secret from it to verfify the code provided from the user
             */

            var userDataObject = Global.user.GetCustomDataObject(Global.userOTPDataObjectName);
            var userData = JsonConvert.DeserializeObject<UserDataObject>(Encoding.UTF8.GetString(userDataObject.Data));
            List<long> timeMatches = userData.TimeStepMatch;

            var totp = new Totp(Base32Encoding.ToBytes(userData.SecurityKey));
            var isValidCode = totp.VerifyTotp(txtVerificationCode.Text, out long timeSetupMatched, VerificationWindow.RfcSpecifiedNetworkDelay);
            // Check to see if the code has been used before
            isValidCode &= !timeMatches.Contains(timeSetupMatched);

            if (isValidCode)
            {
                /* If code user provided is valid then update the users
                 * update the users CDO with the code provided and
                 * allow the user to login
                */
                MessageBox.Show("Valid Code"); //testing only Comment this out for go live

                // This should only keep the last 10 time matches in the list
                // Doing this will help keep the custom user data object smaller in size
                if(timeMatches.Count >= 10)
                {
                    timeMatches.Remove(timeMatches.Count - 1);
                }

                timeMatches.Add(timeSetupMatched);

                string json = JsonConvert.SerializeObject(userData);
                byte[] bytes = Encoding.UTF8.GetBytes(json);

                var encDataObject = new EllieMae.Encompass.BusinessObjects.DataObject();
                encDataObject.Load(bytes);

                Global.user.SaveCustomDataObject(Global.userOTPDataObjectName, encDataObject);
                this.Close(); //Closes the window - should commented out so that I can test codes.
            }
            else
            {
                /* If the code is not valid then only allow 3 times to
                 * provide correct code. If the correct code is not provided
                 * after the 3rd incorrect code Encompass closes.
                */

                if (tryCounter <= 2)
                {
                    MessageBox.Show("Invalid Code");
                    tryCounter += 1;
                }
                else
                {
                    MessageBox.Show("To many faild attempts, closing Encompass.");

                    //Gets the Encompass process and then kills that process.
                    Process p = Process.GetCurrentProcess();
                    p.Kill();
                }
            }
        }

        private void txtVerificationCode_KeyDown(object sender, KeyEventArgs e)
        {
            /* Allows for users to use the enter key to trigger the
             * Verify button
            */

            if (e.KeyCode == Keys.Enter)
            {
                btnVerifyCode_Click(this, new EventArgs());
            }
            else if (e.Alt && e.KeyCode == Keys.F4)
            {
                e.Handled = true;
            }
        }
    }
}
