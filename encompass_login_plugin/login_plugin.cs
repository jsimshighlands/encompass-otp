﻿using System;
using System.Windows.Forms;
using EllieMae.Encompass.ComponentModel;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Users;
using EllieMae.Encompass.BusinessObjects;

using Newtonsoft.Json;

namespace encompass_login_plugin
{
    static class Global
    {
        // Setup globals so to be used for this pluggin
        public static User user;
        public static string userOTPDataObjectName;
    }

    [Plugin]

    public class login_plugin
    {
        private OTPForm optForm;
        private OTPVerify optVerify;

        public login_plugin()
        {
            EncompassApplication.Login += EncompassApplication_Login;
        }

        public void EncompassApplication_Login(object source, EventArgs e)
        {
            /*
             * On Encompass login depending on if the user custom data object
             * exists or not will prompt the user to setup their authenticator.
             * If the user CDO does not exist then open the OTPForm to setup, else
             * open the OTPVerify form to login.
             */
            try
            {
                Global.user = EncompassApplication.Session.GetCurrentUser();
                Global.userOTPDataObjectName = $"{Global.user.ID}_otp.json";

                if (Global.user.ID == "username") // Set this to test user or remove if statement to go live
                {
                    var userDO = Global.user.GetCustomDataObject(Global.userOTPDataObjectName);

                    if (userDO == null)
                    {
                        this.optForm = new OTPForm();
                        this.optForm.ShowDialog();
                    }
                    else
                    {
                        this.optVerify = new OTPVerify();
                        this.optVerify.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }
    }
}
