# Encompass OTP Plugin

## Plugin Overview

* Description:
  * Plugin designed to provide a One Time Password authentication method on login to Encompass. This allows users to use any authenticator they are currently using (i.e. Google Authenticator or Microsoft Authenticator) to log into Encompass.
  * Dependent DLLs:
    * [Opt.NET](https://www.nuget.org/packages/Otp.NET)
    * [QRCoder](https://www.nuget.org/packages/QRCoder/)
  * Additional Encompass Forms:
    * Admin - OTP
      * This form provides a way to clear the user custom data object that holds the one time password key. This will all a user to re-setup an authenticator with a new one time password key in case a phone or authenticator is lost.

## Adding to Encompass

### Plugin

Upload the following dlls into your the Plugins section of the Manage Customization found under the Encompass Input Form Builder:
1. enc_login_plugin.dll - Plugin dll that prompts the user to setup or verify the one time password.
2. Otp.NET.dll - DLL used by the enc_login_plugin to setup or verify one time password codes.
3. QRCoder.dll - DLL used by the enc_login_plugin for the setup screen to generate the QR code used.

### Encompass Form

Import the admin_otp.empkg using the Package Import wizard found in the Encompass Input Form Builder.

## How it works

### Plugin

After the plugin has been added to an Encompass system, the first time a user logs in they will be prompted to setup a One Time Password:

![Setup OTP](images/otp_setup_screen.png)

On successfully verifying the initial code a user custom data object will be created with the schema *username*_otp.json. This json object will keeps track of the secret used to generate the one time password and the last 10 successful passwords used by the user so that repeats cannot used. Users will also be show the below message confirming they have input a correct code:

![Setup Successful](images/setup_successful.png)

Now whenever that users logs into Encompass they will be prompted to verify that login with a verification code before they will be able to use Encompass. The login prompt looks like this:

![Login Prompt](images/otp_login_prompt.png)

If the user inputs the correct code then the user will login to Encompass. If the incorrect code is used or has been used before then the user will receive the following message:

![Invalid Code](images/otp_invalid_code.png)

After 3 invalid codes, the user will receive the below message and Encompass will be closed:

![To many fails](images/otp_to_many_fails.png)

### Encompass Form

The Admin - OTP form looks like below, to remove/reset a users one time password the user's custom data object that holds the secret must be removed. Simple input the user's Encompass ID into the text box and click the "Clear Users OTP - CDO" button. This will remove the json file that holds all the information for that users one time passwords and will prompt the user to setup a new one time password on next Encompass open.

![Encompass Form - Admin - OTP](images/enc_form_admin_otp.png)